OBJS = src/headphoned.o
PROG = headphoned
CFLAGS += `pkg-config --cflags glib-2.0 gconf-2.0 libosso dbus-1 libplayback-1` -Os -Wall
LDFLAGS += `pkg-config --libs glib-2.0 gconf-2.0 libosso dbus-1 libplayback-1`

DESTDIR ?= 
PREFIX ?= /usr

ifdef DEBUG
  CFLAGS += -DHEADPHONED_DEBUG
endif

all: $(PROG)

src/headphoned.o: src/headphoned.c src/config.h

$(PROG): $(OBJS)
	$(CC) $(LDFLAGS) $(LIBS) $^ -o $@
	strip $@

install: $(PROG)
	install $(PROG) $(DESTDIR)$(PREFIX)/sbin/$(PROG)
	install $(PROG).launch $(DESTDIR)$(PREFIX)/sbin/$(PROG).launch
	mkdir -p $(DESTDIR)/etc/$(PROG)
	mkfifo $(DESTDIR)/etc/$(PROG)/mplayer-input
	chown user:users $(DESTDIR)/etc/$(PROG)/mplayer-input
	mkdir -p $(DESTDIR)/etc/event.d/
	install upstart/$(PROG) $(DESTDIR)/etc/event.d/

clean:
	rm -f $(PROG) $(OBJS)

.PHONY: all install clean
.DEFAULT: all

