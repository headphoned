/**
 * headphoned for the Nokia N900
 *
 * The headphone daemon watches the state of the headphone
 * plug (connected, disconnected) and carries out actions
 * based on these events.
 *
 * Contributions:
 *   Faheem Pervez - D-Bus/HAL-based disconnect detection
 *
 * Initial working version: 2009-10-21
 *
 * Copyright (c) 2009-2010 Thomas Perl <thpinfo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this package; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 **/

#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <glib.h>
#include <libosso.h>
#include <dbus/dbus.h>
#include <libplayback/playback.h>
#include <fcntl.h>
#include <stdlib.h>

#include "config.h"

#ifdef HEADPHONED_DEBUG
#    define debug_msg(...) { \
    fprintf(stderr, "DEBUG: "); \
    fprintf(stderr, __VA_ARGS__); \
    fputc('\n',stderr); \
}
#else
#    define debug_msg(...)
#endif

#define warning_msg(...) { \
    fprintf(stderr, "WARNING: "); \
    fprintf(stderr, __VA_ARGS__); \
    fputc('\n', stderr); \
}

/* State file for wired headsets without microphone */
#define STATE_FILE "/sys/devices/platform/gpio-switch/headphone/state"
#define STATE_CONNECTED_STR "connected"
#define STATE_DISCONNECTED_STR "disconnected"

/* Where the HAL Device Manager sits on the D-Bus */
#define HAL_MANAGER_PATH "/org/freedesktop/Hal/Manager"
#define HAL_MANAGER_INTF "org.freedesktop.Hal.Manager"

/* The signal to watch when headphones are removed */
#define HAL_MANAGER_SIGN "DeviceRemoved"

/* A D-Bus rule for filtering events we're interested in */
#define DBUS_RULE_HAL "type='signal',interface='" HAL_MANAGER_INTF \
                      "',path='" HAL_MANAGER_PATH \
                      "',member='" HAL_MANAGER_SIGN "'"

/* The name for headphones (w/ microphone?) on the D-Bus */
#define HEADPHONE_UDI_NAME "/org/freedesktop/Hal/devices/computer_logicaldev_input_1"

#define MCE_SIGNAL_INTF "com.nokia.mce.signal"
#define MCE_SIGNAL_PATH "/com/nokia/mce/signal"
#define MCE_CALL_STATE_SIG "sig_call_state_ind"

#define MCE_CALL_ACTIVE "active"
#define MCE_CALL_RINGING "ringing"
#define MCE_CALL_DISCONNECT "none"

/* A D-Bus rule for getting the call state (active/none/...) */
#define DBUS_RULE_CALL "type='signal',interface='" MCE_SIGNAL_INTF \
                       "',path='" MCE_SIGNAL_PATH \
                       "',member='" MCE_CALL_STATE_SIG "'"

/* Delay in milliseconds for the pause-after-call pause signal */
#define POST_CALL_PAUSE_DELAY_MS 500

/* Where the Media Player backend sits on the D-Bus */
#ifdef DIABLO
#    define MEDIA_SERVER_SRVC "com.nokia.osso_media_server"
#    define MEDIA_SERVER_PATH "/com/nokia/osso_media_server"
#    define MEDIA_SERVER_INTF "com.nokia.osso_media_server.music"
#else
#    define MEDIA_SERVER_SRVC "com.nokia.mafw.renderer.Mafw-Gst-Renderer-Plugin.gstrenderer"
#    define MEDIA_SERVER_PATH "/com/nokia/mafw/renderer/gstrenderer"
#    define MEDIA_SERVER_INTF "com.nokia.mafw.renderer"
#endif

/* Where Panucci sits on the D-Bus */
#define PANUCCI_SRVC "org.panucci.panucciInterface"
#define PANUCCI_PATH "/panucciInterface"
#define PANUCCI_INTF "org.panucci.panucciInterface"

/* MPlayer is not yet 'on the bus', so we use a good old named pipe */
#define MPLAYER_FIFO "/etc/headphoned/mplayer-input"

/* Martin's FM Radio app on the D-Bus */
#define FMRADIO_SRVC "de.pycage.fmradio"
#define FMRADIO_PATH "/de/pycage/fmradio"
#define FMRADIO_INTF "de.pycage.fmradio"

/* Martin's MediaBox on the D-Bus */
#define MEDIABOX_SRVC "de.pycage.mediabox"
#define MEDIABOX_PATH "/de/pycage/mediabox/control"
#define MEDIABOX_INTF "de.pycage.mediabox.control"

typedef struct {
    DBusConnection* session_bus;
    DBusConnection* system_bus;
    osso_context_t* osso;
    pb_playback_t *playback;
    gboolean initial;
    gboolean call_active; /* True while a call is active */
    gboolean unplugged_on_call; /* True when unplugged during active call */
} Headphoned;

/* This has to be globally accessible (for signal handling below) */
static GMainLoop* loop = NULL;

static void
sig_handler(int sig G_GNUC_UNUSED)
{
    debug_msg("Received signal: %d", sig);

    if (loop != NULL && g_main_loop_is_running(loop)) {
        g_main_loop_quit(loop);
    }
}

static void
libplayback_state_request_handler(pb_playback_t   *pb,
                                  enum pb_state_e req_state G_GNUC_UNUSED,
                                  pb_req_t        *req,
                                  void            *data G_GNUC_UNUSED)
{
    pb_playback_req_completed(pb, req);
}

static void
libplayback_state_reply(pb_playback_t   *pb,
                        enum pb_state_e  granted_state G_GNUC_UNUSED,
                        const char      *reason G_GNUC_UNUSED,
                        pb_req_t        *req,
                        void            *data G_GNUC_UNUSED)
{
    pb_playback_req_completed(pb, req);
}

Headphoned*
headphoned_new()
{
    Headphoned* this = g_new0(Headphoned, 1);

    this->osso = osso_initialize("headphoned", "1.0", FALSE, NULL);
    assert(this->osso != NULL);

    this->session_bus = (DBusConnection*)osso_get_dbus_connection(this->osso);
    this->system_bus = (DBusConnection*)osso_get_sys_dbus_connection(this->osso);

    this->playback = pb_playback_new_2(this->session_bus,
                                       PB_CLASS_MEDIA,
                                       PB_FLAG_AUDIO,
                                       PB_STATE_STOP,
                                       (PBStateRequest)libplayback_state_request_handler,
                                       NULL);

    this->initial = TRUE;
    this->call_active = FALSE;
    this->unplugged_on_call = FALSE;

    return this;
}

void
broadcast_pause_signal(Headphoned* headphoned)
{
    int mplayer_fifo;

    debug_msg("Sending pause signal to Media Player");
    /* Nokia Media Player */
    osso_rpc_run(headphoned->osso,
            MEDIA_SERVER_SRVC,
            MEDIA_SERVER_PATH,
            MEDIA_SERVER_INTF,
            "pause",
            NULL,
            DBUS_TYPE_INVALID);
    pb_playback_req_state(headphoned->playback,
                          PB_STATE_STOP,
                          (PBStateReply)libplayback_state_reply,
                          NULL);

    /* Panucci */
    if (dbus_bus_name_has_owner(headphoned->session_bus, PANUCCI_SRVC, NULL)) {
        debug_msg("Sending pause signal to Panucci");
        osso_rpc_run(headphoned->osso,
                PANUCCI_SRVC,
                PANUCCI_PATH,
                PANUCCI_INTF,
                "pause",
                NULL,
                DBUS_TYPE_INVALID);
    } else {
        debug_msg("Panucci not running - not sending pause signal.");
    }

    /* MPlayer */
    if ((mplayer_fifo = open(MPLAYER_FIFO, O_WRONLY | O_NONBLOCK)) != -1) {
        debug_msg("Sending pause signal to MPlayer");
        write(mplayer_fifo, "pause\n", 6);
        close(mplayer_fifo);
    } else {
        debug_msg("MPlayer not running - not sending pause signal.");
    }

    /* FM Radio */
    if (dbus_bus_name_has_owner(headphoned->session_bus, FMRADIO_SRVC, NULL)) {
        debug_msg("Sending pause signal to FM Radio");
        osso_rpc_run(headphoned->osso,
                FMRADIO_SRVC,
                FMRADIO_PATH,
                FMRADIO_INTF,
                "stop",
                NULL,
                DBUS_TYPE_INVALID);
    }

    /* MediaBox */
    if (dbus_bus_name_has_owner(headphoned->session_bus, MEDIABOX_SRVC, NULL)) {
        debug_msg("Sending pause signal to MediaBox");
        osso_rpc_run(headphoned->osso,
                MEDIABOX_SRVC,
                MEDIABOX_PATH,
                MEDIABOX_INTF,
                "stop",
                NULL,
                DBUS_TYPE_INVALID);
    }
}

/* A convenient GSourceFunc wrapper for broadcast_pause_signal */
gboolean
broadcast_pause_signal_later(gpointer data)
{
    Headphoned *headphoned = (Headphoned*)data;
    broadcast_pause_signal(headphoned);
    return FALSE;
}

/* Handler for messages from "wired" headphones (via sysfs) */
gboolean
on_file_changed(GIOChannel* source, GIOCondition condition, gpointer data)
{
    Headphoned* headphoned = (Headphoned*)data;
    gchar* result;

    debug_msg("File %s has changed.", STATE_FILE);

    g_io_channel_seek_position(source, 0, G_SEEK_SET, NULL);
    g_io_channel_read_line(source, &result, NULL, NULL, NULL);
    g_strstrip(result);

    if (headphoned->initial == TRUE) {
        debug_msg("Ignoring initial file change.");
        headphoned->initial = FALSE;
    } else {
        if (g_ascii_strcasecmp(result, STATE_DISCONNECTED_STR) == 0) {
            debug_msg("Broadcasting pause signal (cause: state file)");
            broadcast_pause_signal(headphoned);

            if (headphoned->call_active == TRUE) {
                /**
                 * Remember this unplug event, so that we can
                 * pause players after the call is finished.
                 **/
                debug_msg("Setting flag: unplugged_on_call");
                headphoned->unplugged_on_call = TRUE;
            }
        }
    }

    g_free(result);
    return TRUE;
}

/* Handler for messages from Bluetooth + "wired w/ mic" headphones */
static DBusHandlerResult
on_msg_recieved(DBusConnection* connection G_GNUC_UNUSED, DBusMessage* message, void* data)
{
    Headphoned* headphoned = (Headphoned*)data;
    DBusMessageIter iter;
    const char* result = NULL;

    dbus_message_iter_init(message, &iter);
    dbus_message_iter_get_basic(&iter, &result);

    if (g_str_equal(dbus_message_get_path(message), HAL_MANAGER_PATH)) {
        if (g_str_equal(result, HEADPHONE_UDI_NAME)) {
            debug_msg("Broadcasting pause signal (cause: Hal via D-Bus)");
            broadcast_pause_signal(headphoned);
            return DBUS_HANDLER_RESULT_HANDLED;
        } else {
            return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
        }
    } else if (g_str_equal(dbus_message_get_path(message), MCE_SIGNAL_PATH)) {
        if (g_str_equal(result, MCE_CALL_ACTIVE) ||
                g_str_equal(result, MCE_CALL_RINGING)) {
            debug_msg("Active call detected.");
            headphoned->call_active = TRUE;
            return DBUS_HANDLER_RESULT_HANDLED;
        } else if (g_str_equal(result, MCE_CALL_DISCONNECT)) {
            debug_msg("Call disconnect detected.");
            headphoned->call_active = FALSE;
            if (headphoned->unplugged_on_call == TRUE) {
                headphoned->unplugged_on_call = FALSE;
                debug_msg("Broadcasting pause signal (cause: call disconnect)");
                g_timeout_add(POST_CALL_PAUSE_DELAY_MS,
                        broadcast_pause_signal_later,
                        (gpointer)headphoned);
            }
            return DBUS_HANDLER_RESULT_HANDLED;
        } else {
            return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
        }
    }

    return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
}

int
main(int argc, char* argv[])
{
    Headphoned *headphoned = NULL;
    GIOChannel* state = NULL;

    signal(SIGINT, sig_handler);
    signal(SIGQUIT, sig_handler);
    signal(SIGTERM, sig_handler);

    loop = g_main_loop_new(NULL, FALSE);
    headphoned = headphoned_new();

    state = g_io_channel_new_file(STATE_FILE, "r", NULL);
    if (state != NULL) {
        debug_msg("Adding I/O watch on %s", STATE_FILE);
        g_io_add_watch(state, G_IO_PRI, on_file_changed, headphoned);
    } else {
        warning_msg("Cannot open state file: %s", STATE_FILE);
    }

    debug_msg("Registering D-Bus rule: %s", DBUS_RULE_HAL);
    dbus_bus_add_match(headphoned->system_bus, DBUS_RULE_HAL, NULL);

    debug_msg("Registering D-Bus rule: %s", DBUS_RULE_CALL);
    dbus_bus_add_match(headphoned->system_bus, DBUS_RULE_CALL, NULL);

    dbus_connection_add_filter(headphoned->system_bus, on_msg_recieved, headphoned, NULL);

    debug_msg("Entering GLib main loop...");
    g_main_loop_run(loop);
    debug_msg("...main loop finished. Cleaning up.");

    if (state != NULL) {
        g_io_channel_unref(state);
    }

    pb_playback_destroy(headphoned->playback);
    osso_deinitialize(headphoned->osso);
    g_free(headphoned);

    return EXIT_SUCCESS;
}

